/*         semantique.h*/
/*
*  This file is part of PRAELECTOR.
*
*  PRAELECTOR is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  COLLATINVS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with PRAELECTOR; if not, write to the Free Software
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* © Yves Ouvrard, 2009 - 2019
*/

#ifndef SEMANTIQUE_H
#define SEMANTIQUE_H

#include <QMap>
#include <QObject>
#include <QString>

class Lien;
class Semantique;

class Seme: public QObject
{
    Q_OBJECT

    private:
        QString          _cle;
        QList<Lien*>     _lSynt;
        QList<Lien*>     _lOnto;
        Semantique*      _rete;
    public:
        Seme(QString c, QObject* parent=0);
        //void        aj(QString op, Seme* s);
        //void        aj(Lien* l);
        void        ajLin(QString op, Seme* s);
        bool        aLien(Lien* l);
        QString     cle();
        bool        hyponyme(Seme* s);
        //bool        monteLien(Lien* l);
        int         nbOnto();
        Lien*       onto(int i);
        void        rmLien(Lien* l);
        QString     toString();
};

bool operator==(Seme& sa, Seme& sb);

class Lien: public QObject
{
    Q_OBJECT

    private:
        Seme*       _sup;
        QString     _id;
    public:
        Lien(QString idl, Seme* s);
        Seme*       sup();
        QString     id();
        bool        hypo();
        QString     toString();
};

bool operator==(Lien& la, Lien& lb);

class Semantique: public QObject
{

    Q_OBJECT

    private:
        QString              _chemin;
        QMap<QString, Seme*> _semes;
    public:
        Semantique();
        void  ajLien(QString l);
        int   nbSemes();
        QList<Seme*> ontLien(Lien* l);
        void  sauve();
        Seme* seme(QString c);
        Seme* seme(int i);
};

#endif
