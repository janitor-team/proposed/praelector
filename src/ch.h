/*       ch.h     */

#ifndef CH_H
#define CH_H

#include <QMap>
#include <QRegExp>
#include <QStandardPaths>
#include <QString>
#include <QStringList>

namespace Ch
{
    QStringList const abrev  = QStringList()
        <<"Agr"<<"Ap"<<"A"<<"K"<<"D"<<"F"<<"C"
        <<"Cn"<<"L"<<"Mam"<<"M\""<<"M"<<"N"<<"Oct"
        <<"Opet"<<"Post"<<"Pro"<<"P"<<"Q"<<"Sert"
        <<"Ser"<<"Sex"<<"S"<<"St"<<"Ti"<<"T"<<"V"
        <<"Vol"<<"Vop"<<"Pl";
    QStringList const anteposes = QStringList()
        << "la"
        << "le"
        << "les"
        << "leur"
        << "lui"
        << "me"
        << "nous"
        << "se"
        << "te"
        << "vous";
    QStringList const cas = QStringList()
        << "nominatif"
        << "vocatif"
        << "accusatif"
        << "génitif"
        << "datif"
        << "ablatif"
        << "locatif";
    QStringList const genres = QStringList()
        << "masculin"
        << "féminin"
        << "neutre";
    QStringList const personnes = QStringList()
        <<"1ère"
        <<"2ème"
        <<"3ème";
    QStringList const modes = QStringList()
        << "indicatif"
        << "subjonctif"
        << "impératif"
        << "infinitif"
        << "participe"
        << "gérondif"
        << "adjectif verbal";
    QStringList const nombres = QStringList()
        << "singulier"
        << "pluriel";
    QString const     consonnes = "bcdfgjklmnpqrstvxz";
    // couleurs
    QString const     noir = "000000";
    QString const     ocre = "800000";
    QString const     vert = "00cc00";
    QString const     bleu = "0099cc";

    QString           accentue(QString l);
    void              allonge(QString *f);
    QString           atone(const QString a, bool bdc = false);
    //QStringList       _chemins = QStandardPaths::standardLocations(QStandardPaths::AppDataLocation);
    QString           chemin(QString f);
    void              deQuant(QString *c);
    QString           deramise(QString r);
    void              elide(QString *mp);
    QString           jviu(QString ch);
    QString           ote_diacritiques (QString k);
    const QRegExp     reAlphas("(\\w+)");
    const QRegExp     reEspace("\\s+");
    const QRegExp     reLettres("\\w");
    const QRegExp     rePonct("([\\.?!;:]|\\n\\n)");
    QChar const       separSyll = 0x02CC;
    QString           transforme(QString k);
    QString           versPC(QString k);
    QString           versPedeCerto(QString k);
    QString const     voyelles = "āăēĕīĭōŏūŭȳўĀĂĒĔĪĬŌŎŪŬȲЎ";
}
#endif
